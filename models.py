from app import db

class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    address = db.Column(db.String(500))
    email = db.Column(db.String(500))
    phone = db.Column(db.Integer)
    username = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(500))
    admin = db.Column(db.Integer)
    purchases = db.relationship("Purchase", backref = "client", lazy = "dynamic")

    def __repr__(self):
            return '{}{}{}{}{}{}{}'.format(self.id, self.name, self.address, self.email, self.phone, self.username, self.password)

class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    description = db.Column(db.String(500))
    year = db.Column(db.Integer)
    author = db.Column(db.String(50))
    availability = db.Column(db.Integer)
    price = db.Column(db.Float)
    purchased_books = db.relationship("Purchased_books", backref = "book", lazy = "dynamic")

    def __repr__(self):
            return '{}{}{}{}{}{}'.format(self.id, self.title, self.description, self.year, self.author, self.availability)

class Purchase(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'))
    date_time = db.Column(db.DateTime)
    purchased_books = db.relationship("Purchased_books", backref = "purchase", lazy = "dynamic")

    def __repr__(self):
            return '{}{}'.format(self.id, self.date_time)

class Purchased_books(db.Model):
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'), primary_key=True)
    purchase_id = db.Column(db.Integer, db.ForeignKey('purchase.id'), primary_key=True)

    def __repr__(self):
            return '{}{}'.format(self.book_id, self.purchase_id)