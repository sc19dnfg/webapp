from flask import render_template, request, url_for, redirect, session
from app import app
from app import db, models 
from datetime import datetime
from .forms import *
import logging

@app.route('/')
def index(): #shows the homepage
    app.logger.info('entered the homepage') 

    t = models.Book.query.all()
    
    return render_template('index.html', t=t)
    

@app.route('/userspace')
def userspace(): #shows the books
    app.logger.info('user accessed their own space')

    id_list = []
    p = models.Purchase.query.filter_by(client_id=session['id']).all()

    for pa in p:
        id_list.append(pa.id)
    d = models.Purchased_books.query.filter(models.Purchased_books.purchase_id.in_(id_list)).all()
    id_list = []
    for da in d:
        id_list.append(da.book_id)
    t = models.Book.query.filter(models.Book.id.in_(id_list)).all()
    return render_template('userspace.html', t=t)

@app.route('/books')
def books(): #shows the books
    app.logger.debug('access to admin table books')
    t = models.Book.query.all()
    return render_template('books.html', t=t)

@app.route('/allbooks')
def allbooks(): #shows the books
    app.logger.info('accessed the entire list of books')
    t = models.Book.query.all()
    return render_template('allbooks.html', t=t)

@app.route('/addbook', methods=['GET', 'POST'])
def addbook():
    form = FBook()
    app.logger.info('admin accessed form to add a book')
    if request.method == 'POST' : #in case there is a value returned
        p = models.Book(title = form.title.data, description=form.description.data, year=form.year.data, author=form.author.data, price=form.price.data, availability = form.availability.data)
        db.session.add(p)
        db.session.commit()
        return redirect('/') #goes to homepage
    else: #in case there isn't
        return render_template('addbook.html', form=form)

@app.route('/clients')
def clients(): #shows the books
    app.logger.debug('access to admin table clients')
    t = models.Client.query.all()
    return render_template('clients.html', t=t)

@app.route('/addclient', methods=['GET', 'POST'])
def addclient():
    form = FClient()
    app.logger.info('admin tried to add client manually')
    if request.method == 'POST' : #in case there is a value returned
        p = models.Client(name = form.name.data, address=form.address.data, email=form.email.data, phone=form.phone.data, username = form.username.data, password = form.password.data, admin = form.admin.data)
        db.session.add(p)
        db.session.commit()
        return redirect('/') #goes to homepage
    else: #in case there isn't
        return render_template('addclient.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = FLogin()

    if request.method == 'POST' : #in case there is a value returned
        t = models.Client.query.filter_by(username=form.username.data)
        if t[0].password == form.password.data:
            session['id'] = t[0].id
            session['type'] = t[0].admin
            session['login'] = 1
            the_order_list = []
            session ['cart'] = the_order_list
            session['price'] = 0
            app.logger.info('successful login')
            return redirect('/') #goes to homepage
        else:
            app.logger.warning('warning: user inputted wrong password')
            return redirect('/login')
    else: #in case there isn't
        return render_template('login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = FRegister()

    if request.method == 'POST' : #in case there is a value returned
        p = models.Client(name = form.name.data, address=form.address.data, email=form.email.data, phone=form.phone.data, username = form.username.data, password = form.password.data, admin = 0)
        db.session.add(p)
        db.session.commit()
        app.logger.info('new user tried to register')
        return redirect('/') #goes to homepage
    else: #in case there isn't
        return render_template('register.html', form=form)

@app.route('/logout', methods=['GET', 'POST'])
def logout():
    session['id'] = -1
    session['type'] = 0
    session['login'] = -1
    the_order_list = []
    session ['cart'] = the_order_list
    session['price'] = 0
    app.logger.info('user logged out')
    return redirect('/') #goes to homepage

@app.route('/cart', methods=['GET', 'POST'])
def cart():
    app.logger.info('user accessed the cart')
    the_order_list = session['cart']
    sum = session['price']
    t = models.Book.query.filter(models.Book.id.in_(the_order_list)).all()
    return render_template('cart.html', t = t, sum = sum)

@app.route('/addtocart/<int:id>')
def addtocart(id):
    app.logger.info('an item was added to the cart')
    t = models.Book.query.get(id)
    the_order_list = session['cart']
    sum = session['price']

    the_order_list.append(id)
    session['cart'] = the_order_list
    sum = sum + t.price
    session['price'] = sum

    return redirect('/')

@app.route('/purchase', methods=['GET', 'POST'])
def purchase():
    app.logger.info('a purchase has been made')
    p = models.Purchase(client_id = session['id'], date_time = datetime.now())
    db.session.add(p)
    db.session.commit()
    id = p.id

    the_order_list = session['cart']

    for ta in the_order_list:
        c = models.Purchased_books(book_id= ta, purchase_id = 1)
        db.session.add(c)
        db.session.commit()
    the_order_list = []
    session ['cart'] = the_order_list
    session['price'] = 0
    return redirect('/') #goes to homepage

@app.route('/password', methods=['GET', 'POST'])
def password():

    form = FPassword()
    app.logger.info('a password has been changed')
    if request.method == 'POST' : #in case there is a value returned
        c = models.Client.query.get(session['id'])
        c.password = form.password.data
        db.session.commit()
        return redirect('/') #goes to homepage
    else: #in case there isn't
        return render_template('password.html', form=form)

@app.route('/individualbook/<int:id>')
def individualbook(id): #shows the books
    app.logger.info('entered the full description of a book')
    t = models.Book.query.get(id)
    return render_template('individualbook.html', t=t)
    
@app.route('/purchases')
def purchases(): #shows the books
    app.logger.debug('admin accessed the recent purchases')
    t = models.Purchase.query.all()
    return render_template('purchases.html', t=t)

@app.route('/purchased_books')
def purchased_books(): #shows the books
    app.logger.debug('admin accessed the elements of a purchase')
    t = models.Purchased_books.query.all()
    return render_template('purchased_books.html', t=t)

@app.route('/remove/<int:id>')
def remove(id):
    app.logger.info('an item was removed from the cart')
    the_order_list = session['cart']
    the_order_list.remove(id)
    session['cart'] = the_order_list

    return redirect('/cart')

